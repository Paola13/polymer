//version inicial

var express = require('express'),
  cors=require('cors'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
//var cors = require('cors');
app.options('*',cors());
//app.use(cors());

app.use(express.static(__dirname + "/build/default"));


app.listen(port);

console.log('Proyecto Polymer con wrapper de NodeJs con: ' + port);

app.get('/', function(req,res){
  res.sendFile('index.html',{root:'.'});
})
